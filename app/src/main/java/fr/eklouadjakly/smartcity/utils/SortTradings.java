package fr.eklouadjakly.smartcity.utils;

import com.google.android.gms.maps.model.LatLng;

import java.util.Comparator;

import fr.eklouadjakly.smartcity.model.Trading;

public class SortTradings implements Comparator<Trading> {

    LatLng currentLoc;

    public SortTradings(LatLng currentLoc) {
        this.currentLoc = currentLoc;
    }

    @Override
    public int compare(final Trading place1, final Trading place2) {
        double lat1 = place1.getLatitude() == null ? -1 : Double.parseDouble(place1.getLatitude());
        double lon1 = place1.getLongitude() == null ? -1 : Double.parseDouble(place1.getLongitude());
        double lat2 = place2.getLatitude() == null ? -1 : Double.parseDouble(place2.getLatitude());
        double lon2 = place2.getLongitude() == null ? -1 : Double.parseDouble(place2.getLongitude());

        double distanceToPlace1 = distance(currentLoc.latitude, currentLoc.longitude, lat1, lon1);
        double distanceToPlace2 = distance(currentLoc.latitude, currentLoc.longitude, lat2, lon2);
        return (int) (distanceToPlace1 - distanceToPlace2);
    }

    public double distance(double fromLat, double fromLon, double toLat, double toLon) {
        double radius = 6378137;   // approximate Earth radius, *in meters*
        double deltaLat = toLat - fromLat;
        double deltaLon = toLon - fromLon;
        double angle = 2 * Math.asin( Math.sqrt(
                Math.pow(Math.sin(deltaLat/2), 2) +
                        Math.cos(fromLat) * Math.cos(toLat) *
                                Math.pow(Math.sin(deltaLon/2), 2) ) );
        return radius * angle;
    }
}
