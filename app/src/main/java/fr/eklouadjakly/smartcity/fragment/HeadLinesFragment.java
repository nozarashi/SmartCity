package fr.eklouadjakly.smartcity.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.adapter.NewsAdapter;
import fr.eklouadjakly.smartcity.model.Article;
import fr.eklouadjakly.smartcity.model.NewsApi;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.ApiService;
import fr.eklouadjakly.smartcity.network.NewsApiError;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import fr.eklouadjakly.smartcity.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HeadLinesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HeadLinesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HeadLinesFragment extends Fragment {

    private static final String TAG = "HeadLinesFragment";
    ApiService service;
    Call<NewsApi> call;
    TokenManager tokenManager;
    User user;

    private int totalResults;
    private List<Article> articles = new ArrayList<>();
    private RecyclerView recyclerView;
    private ProgressBar mProgressView;
    private TextView errorView;

    private OnFragmentInteractionListener mListener;

    public HeadLinesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HeadLinesFragment.
     */
    public static HeadLinesFragment newInstance() {
        HeadLinesFragment fragment = new HeadLinesFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tokenManager = TokenManager.getInstance(getActivity().getSharedPreferences("prefs", Context.MODE_PRIVATE));
        user = tokenManager.getUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_head_lines, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = getActivity().findViewById(R.id.newsrv);
        mProgressView = getActivity().findViewById(R.id.news_progress);
        errorView = getActivity().findViewById(R.id.newserror);
        service = RetrofitBuilder.createServiceForNewsApi(ApiService.class, tokenManager);

        //Obtenir les centres d'intérêts de l'utilisateur
        final String interests;
        String userInterests = getInterests();
        Log.w(TAG, "onViewCreated: interests " + userInterests);
        if (userInterests == null || userInterests.isEmpty()) {
            Log.i(TAG, "onViewCreated: no interests");
            errorView.setText(R.string.no_interest_msg);
            return;
        }

        if (user.getLocation().getCity() != null) {
            interests = user.getLocation().getCity() + " OR " + userInterests;
        } else {
            interests = userInterests;
        }
        Log.w(TAG, "onViewCreated interest: " + interests);
        //On veut les articles publiés entre hier et aujourd'hui
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        Date d = cal.getTime();
        final String date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(d);

        //La langue de l'utilisateur
        Locale current = getResources().getConfiguration().locale;
        final String language = "" + current.getLanguage().charAt(0) + current.getLanguage().charAt(1);

        call = service.news(language, interests, date, 100, 1);
        showProgress(true);
        call.enqueue(new Callback<NewsApi>() {
            @Override
            public void onResponse(Call<NewsApi> call, Response<NewsApi> response) {
                if (response.isSuccessful()) {
                    if (!response.body().getArticles().isEmpty()) {
                        Log.w(TAG, "onResponse: " + response.body().getArticles().get(0).getDescription());
                        articles.addAll(response.body().getArticles());
                        totalResults = response.body().getTotalResults();
                        setNewsAdapter(articles);
                        showProgress(false);
                        int i = 2;
                        /*if (totalResults > 100) {
                            // on récupère les données restantes
                            int remaining = totalResults - 100;
                            while (remaining > 0 && i++ <= 3) {
                                call = service.news(language, interests, date, 100, i);
                                remaining -= 100;
                                call.enqueue(new Callback<NewsApi>() {
                                    @Override
                                    public void onResponse(Call<NewsApi> call, Response<NewsApi> response) {
                                        if (response.isSuccessful()) {
                                            articles.addAll(response.body().getArticles());
                                            recyclerView.getAdapter().notifyDataSetChanged();
                                        } else {
                                            NewsApiError apiError = Utils.convertNewsErrors(response.errorBody());
                                            Log.w(TAG, "onResponse error: " + apiError.message);

                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<NewsApi> call, Throwable t) {
                                        Log.e(TAG, "onFailure: " + t.getMessage());
                                    }
                                });

                            }
                        }*/
                    } else {
                        //setNewsAdapter(articles);
                        showProgress(false);
                    }
                } else {
                    NewsApiError apiError = Utils.convertNewsErrors(response.errorBody());
                    Log.w(TAG, "onResponse error: " + apiError.message);
                }
            }

            @Override
            public void onFailure(Call<NewsApi> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    private void setNewsAdapter(List<Article> list) {
        NewsAdapter myAdapter = new NewsAdapter(getContext(), list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(myAdapter);
    }


    private String getInterests() {
        List<String> interests = new ArrayList<>();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext());
        String[] interestsItems = getResources().getStringArray(R.array.interests_items);
        for (String item : interestsItems) {
            boolean isChecked = preferences.getBoolean(item, false);
            if (isChecked) {
                interests.add(item);
            }
        }
        return TextUtils.join(" OR ", interests);
    }

    /**
     * Shows the progress UI and hides the goToSignin form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
            recyclerView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (call != null) {
            call.cancel();
            call = null;
        }
    }
}
