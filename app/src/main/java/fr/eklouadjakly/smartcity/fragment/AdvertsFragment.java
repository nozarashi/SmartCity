package fr.eklouadjakly.smartcity.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.activity.LoginActivity;
import fr.eklouadjakly.smartcity.adapter.AdvertsAdapter;
import fr.eklouadjakly.smartcity.model.Advert;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.ApiError;
import fr.eklouadjakly.smartcity.network.ApiService;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import fr.eklouadjakly.smartcity.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AdvertsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AdvertsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AdvertsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    private static final String TAG = "AdvertsFragment";
    ApiService service;
    Call<List<Advert>> call;
    TokenManager tokenManager;
    User user;

    SwipeRefreshLayout swipeRefreshLayout;
    private List<Advert> adverts = new ArrayList<>();
    private RecyclerView recyclerView;
    private AdvertsAdapter myAdapter;

    private OnFragmentInteractionListener mListener;

    public AdvertsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AdvertsFragment.
     */
    public static AdvertsFragment newInstance(String param1, String param2) {
        AdvertsFragment fragment = new AdvertsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tokenManager = TokenManager.getInstance(getActivity().getSharedPreferences("prefs", Context.MODE_PRIVATE));
        user = tokenManager.getUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_adverts, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefreshLayout = getActivity().findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            swipeRefreshLayout.setProgressViewOffset(false, 0, 500);
        }
        recyclerView = getActivity().findViewById(R.id.advertsrv);
        service = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);

        loadRecyclerViewData();
    }

    private void loadRecyclerViewData() {
        swipeRefreshLayout.setRefreshing(true);
        call = service.getAdverts(user.getId());

        call.enqueue(new Callback<List<Advert>>() {
            @Override
            public void onResponse(Call<List<Advert>> call, Response<List<Advert>> response) {
                if (response.isSuccessful()) {
                    Log.w(TAG, "onResponse: success "+response.body());
                    if (!response.body().isEmpty()) {
                        adverts.clear();
                        adverts.addAll(response.body());
                        Log.w(TAG, "advert: "+adverts.size() );
                        setAdvertsAdapter(adverts);
                    }
                } else {
                    if (response.code() == 401) {
                        tokenManager.deleteToken();
                        tokenManager.removeUser();
                        tokenManager.removeUUID();

                        getActivity().finish();
                        startActivity(new Intent(getContext(), LoginActivity.class));
                    }
                    ApiError apiError = Utils.convertErrors(response.errorBody());
                    Log.w(TAG, "onResponse error : " + apiError.getMessage());
                    Toast.makeText(getActivity(), apiError.getMessage(), Toast.LENGTH_LONG).show();
                }
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<List<Advert>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
                swipeRefreshLayout.setRefreshing(false);

            }
        });
    }

    private void setAdvertsAdapter(List<Advert> list) {
        myAdapter = new AdvertsAdapter(getContext(), list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(myAdapter);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        loadRecyclerViewData();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
