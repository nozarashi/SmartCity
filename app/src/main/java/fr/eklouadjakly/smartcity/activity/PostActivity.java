package fr.eklouadjakly.smartcity.activity;

import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import fr.eklouadjakly.smartcity.GlideApp;
import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.adapter.CommentsAdapter;
import fr.eklouadjakly.smartcity.adapter.PostsAdapter;
import fr.eklouadjakly.smartcity.model.Comment;
import fr.eklouadjakly.smartcity.model.Post;
import fr.eklouadjakly.smartcity.network.ApiService;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.CircleTransform;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class PostActivity extends AppCompatActivity {


    TextView username, createdAt, postContent, nbComments;
    ImageView profileView;
    RecyclerView recyclerView;

    CommentsAdapter myAdapter;

    AppBarLayout appbar;
    Toolbar toolbar;
    Post post;
    TokenManager tokenManager;
    ApiService service;
    Call<Post> call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        tokenManager = TokenManager.getInstance(getSharedPreferences("prefs", MODE_PRIVATE));
        service = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);

        String postAsString = getIntent().getStringExtra(PostsAdapter.POST_DATA);
        post = new Gson().fromJson(postAsString, Post.class);
        username = findViewById(R.id.post_username);
        createdAt = findViewById(R.id.post_createdAt);
        postContent = findViewById(R.id.post_content);
        nbComments = findViewById(R.id.comment_number);
        profileView = findViewById(R.id.post_user_profile);

        //Setup toolbar
        appbar = findViewById(R.id.appbar);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(post.getTitle());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        String profileUrl = RetrofitBuilder.getRemoteUrl() + "img/" + post.getUser().getPicture();
        String name = post.getUser().getFirstname() + " " + post.getUser().getLastname();
        String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

        DateFormat df = DateFormat.getDateTimeInstance();
        try {
            Date d = new SimpleDateFormat(DATE_FORMAT_PATTERN).parse(post.getCreatedAt().substring(0, 19));
            String date = df.format(d);
            createdAt.setText(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        username.setText(name);
        postContent.setText(post.getContent());
        nbComments.setText(Integer.toString(post.getComments().size()));
        GlideApp.with(this)
                .load(profileUrl)
                .apply(new RequestOptions().format(DecodeFormat.PREFER_RGB_565))
                .transition(withCrossFade())
                .transform(new CircleTransform(this))
                .into(profileView);

        recyclerView = findViewById(R.id.commentsrv);
        recyclerView.setFocusable(false);
        findViewById(R.id.post_details).requestFocus();
        setCommentsAdapter(post.getComments());

        // Comment
        final EditText editTextComment = findViewById(R.id.edittext_comment);
        ImageView addComment = findViewById(R.id.add_comment);
        addComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String content = editTextComment.getText().toString();
                call = service.comment(post.getId(), content);
                call.enqueue(new Callback<Post>() {
                    @Override
                    public void onResponse(Call<Post> call, Response<Post> response) {
                        if (response.isSuccessful()) {
                            post = response.body();
                            myAdapter.setData(post.getComments());
                            myAdapter.notifyDataSetChanged();
                            nbComments.setText(Integer.toString(post.getComments().size()));
                        }
                    }

                    @Override
                    public void onFailure(Call<Post> call, Throwable t) {

                    }
                });
            }
        });
    }

    private void setCommentsAdapter(List<Comment> list) {
        myAdapter = new CommentsAdapter(this, list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(myAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
