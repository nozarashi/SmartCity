package fr.eklouadjakly.smartcity.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.adapter.OfferAdapter;
import fr.eklouadjakly.smartcity.model.Offer;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.ApiService;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Faniry on 01/05/2018.
 */

public class ShopsOfferActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    Toolbar toolbar;
    TokenManager tokenManager;
    private static final String TAG = "ShopsOfferActivity";
    SwipeRefreshLayout swipeRefreshLayout;
    private OfferAdapter mAdapter;
    private RecyclerView mRecyclerView;
    Call<List<Offer>> call;

    private List<Offer> customOffers = new ArrayList<>();
    private List<Offer> allOffers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_offer);

        swipeRefreshLayout = findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            swipeRefreshLayout.setProgressViewOffset(false, 0, 500);
        }

        //Setup toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.menu_offer));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //on récupère les infos de l'utilisateur
        User user;
        tokenManager = TokenManager.getInstance(getSharedPreferences("prefs", MODE_PRIVATE));
        user = tokenManager.getUser();


        if (user != null) {

            loadRecyclerViewData();
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.listshowoffer);
        mRecyclerView.setHasFixedSize(true);
    }

    private void setOfferAdapter(List<Offer> list) {
        mAdapter = new OfferAdapter(this, list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.offers_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.offers_search:
                startActivity(new Intent(this, OffersSearchActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        loadRecyclerViewData();
    }

    public void loadRecyclerViewData() {
        swipeRefreshLayout.setRefreshing(true);
        ApiService service = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);
        call = service.getOffers();
        call.enqueue(new Callback<List<Offer>>() {

            @Override
            public void onResponse(Call<List<Offer>> call, Response<List<Offer>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEmpty()) {
                            Toast.makeText(ShopsOfferActivity.this, "No results", Toast.LENGTH_LONG).show();
                        } else {
                            customOffers.clear();
                            customOffers.addAll(response.body());
                            setOfferAdapter(customOffers);
                        }
                    } else {
                        Toast.makeText(ShopsOfferActivity.this, "Error", Toast.LENGTH_LONG).show();
                    }
                }
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<List<Offer>> call, Throwable t) {
                Toast.makeText(ShopsOfferActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

}
