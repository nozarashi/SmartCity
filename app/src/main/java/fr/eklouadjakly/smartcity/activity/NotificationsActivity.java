package fr.eklouadjakly.smartcity.activity;

import android.content.Context;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.adapter.GroupsAdapter;
import fr.eklouadjakly.smartcity.adapter.NotificationsAdapter;
import fr.eklouadjakly.smartcity.model.Group;
import fr.eklouadjakly.smartcity.model.Notification;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.ApiService;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    Toolbar toolbar;
    RecyclerView recyclerView;
    NotificationsAdapter myAdapter;

    ApiService service;
    Call<List<Notification>> call;
    TokenManager tokenManager;
    User user;

    SwipeRefreshLayout swipeRefreshLayout;

    private List<Notification> notifications = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        //Setup toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.notifications));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        swipeRefreshLayout = this.findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            swipeRefreshLayout.setProgressViewOffset(false, 0, 500);
        }
        recyclerView = findViewById(R.id.notificationsrv);

        tokenManager = TokenManager.getInstance(this.getSharedPreferences("prefs", Context.MODE_PRIVATE));
        user = tokenManager.getUser();
        service = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);

        loadRecyclerViewData();
    }

    private void loadRecyclerViewData() {
        swipeRefreshLayout.setRefreshing(true);
        call = service.getNotifications();
        call.enqueue(new Callback<List<Notification>>() {
            @Override
            public void onResponse(Call<List<Notification>> call, Response<List<Notification>> response) {
                if(response.isSuccessful()) {
                    if(response.body() != null) {
                        if (response.body().isEmpty()) {
                            Toast.makeText(NotificationsActivity.this, "No results", Toast.LENGTH_LONG).show();
                        } else {
                            notifications.clear();
                            notifications.addAll(response.body());
                            setNotifsAdapter(notifications);
                        }
                    } else {
                        Toast.makeText(NotificationsActivity.this, "Error", Toast.LENGTH_LONG).show();
                    }
                }
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<List<Notification>> call, Throwable t) {
                Toast.makeText(NotificationsActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    /**
     * Initialise l'adapteur
     * @param list liste de groupes
     */
    private void setNotifsAdapter(List<Notification> list) {
        myAdapter = new NotificationsAdapter(this, list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(myAdapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (call != null) {
            call.cancel();
            call = null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        loadRecyclerViewData();
    }
}
