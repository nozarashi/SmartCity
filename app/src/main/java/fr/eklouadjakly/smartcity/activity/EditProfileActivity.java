package fr.eklouadjakly.smartcity.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.maps.android.SphericalUtil;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.eklouadjakly.smartcity.GlideApp;
import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.ApiError;
import fr.eklouadjakly.smartcity.network.ApiService;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.CircleTransform;
import fr.eklouadjakly.smartcity.utils.FilePath;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import fr.eklouadjakly.smartcity.utils.Utils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private static final String TAG = "EditProfileActivity";
    private static final int PICK_FILE_REQUEST = 1;
    private static final int REQUEST_ACCESS_LOCATION = 0;
    private static final double RADIUS = 5000;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 228;
    private static final int REQUEST_READ_EXTERNAL_STORAGE = 2;
    private static LatLngBounds BOUNDS = null;
    private static String COUNTRY = null;
    private boolean canUpdate = false;
    FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;

    Toolbar toolbar;

    @BindView(R.id.edit_profile_ll)
    LinearLayout editProfilell;
    @BindView(R.id.firstname)
    TextInputLayout tilFirstname;
    @BindView(R.id.lastname)
    TextInputLayout tilLastname;
    @BindView(R.id.city)
    EditText tilCity;
    @BindView(R.id.profilePicture)
    ImageView profilePictureView;
    DateFormat dateFormat;
    @BindView(R.id.birthday)
    EditText tilBirthday;
    @BindView(R.id.attachment)
    TextView attachment;
    DatePickerDialog datePickerDialog;
    AutocompleteFilter typeFilter;

    private String selectedFilePath;
    private final String[] okFileExtensions =  new String[] {"jpg", "png", "gif","jpeg"};

    TokenManager tokenManager;
    User user;
    String profileUrl;
    ApiService service;
    Call<User> call;

    AwesomeValidation validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        //Location permission
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mayRequestPermission();
        //createLocationRequest();
        initLocation();

        //Setup toolbar
        toolbar = findViewById(R.id.toolbar);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.menu_profile));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tokenManager = TokenManager.getInstance(getSharedPreferences("prefs", MODE_PRIVATE));
        user = tokenManager.getUser();
        tilFirstname.getEditText().setText(user.getFirstname());
        tilLastname.getEditText().setText(user.getLastname());
        String city = user.getLocation().getCity() + ", " + user.getLocation().getCountry();
        tilCity.setText(city);

        DateFormat format = DateFormat.getDateInstance();
        try {
            if (user.getDateOfBirth() != null) {
                Date d = new SimpleDateFormat("yyyy-MM-dd").parse(user.getDateOfBirth());
                tilBirthday.setText(format.format(d));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        profileUrl = RetrofitBuilder.getRemoteUrl() + "img/" + user.getPicture();
        GlideApp.with(this)
                .load(profileUrl)
                .signature(new ObjectKey(tokenManager.getUUID()))
                .apply(new RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                .transition(withCrossFade())
                .transform(new CircleTransform(this))
                //  .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(profilePictureView);


        //typeFilter = AutocompleteFilter.create(filters);


        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                Location location = locationResult.getLocations().get(0);
                BOUNDS = toBounds(new LatLng(location.getLatitude(), location.getLongitude()), RADIUS);
                updateLocationData(location);
                Log.i(TAG, "onLocationResult: " + location.getLatitude() + ", " + location.getLongitude());
            }
        };

        service = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);

        dateFormat = DateFormat.getDateInstance();

        setDateTimeField();


        validator = new AwesomeValidation(ValidationStyle.TEXT_INPUT_LAYOUT);
        setupRules();
    }

    public void setupRules() {
        validator.addValidation(this, R.id.firstname, RegexTemplate.NOT_EMPTY, R.string.error_field_required);
        validator.addValidation(this, R.id.lastname, RegexTemplate.NOT_EMPTY, R.string.error_field_required);
    }

    private void setDateTimeField() {
        tilBirthday.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();

        datePickerDialog = new SpinnerDatePickerDialogBuilder()
                .context(this)
                .callback(this)
                .spinnerTheme(R.style.NumberPickerStyle)
                .showTitle(true)
                .defaultDate(newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH))
                .build()
        ;
    }

    @Override
    public void onClick(View v) {
        if (v == tilBirthday) {
            datePickerDialog.show();
        } else if (v == attachment) {
            showFileChooser();
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.save_profile:
                saveProfile();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void saveProfile() {
        tilFirstname.setError(null);
        tilLastname.setError(null);
        validator.clear();
        if (validator.validate()) {
            call = service.patchUser(user.getId(), user);
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (response.isSuccessful()) {
                        Log.w(TAG, "onResponse success: " + response.body());
                        tokenManager.saveUser(user);
                        finish();
                    } else {
                        ApiError apiError = Utils.convertErrors(response.errorBody());
                        Log.w(TAG, "onResponse failure: " + apiError.getMessage() + " " + response.code());
                        Toast.makeText(EditProfileActivity.this, apiError.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Log.w(TAG, "onFailure: " + t.getMessage());
                    Toast.makeText(EditProfileActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @OnClick(R.id.city)
    public void findPlace(View view) {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setBoundsBias(BOUNDS)
                            .setFilter(typeFilter)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.attachment)
    void upload() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            showFileChooser();

        } else if (checkSelfPermission(READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            showFileChooser();
        } else {
            if (shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE)) {
                Snackbar.make(editProfilell, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                        .setAction(android.R.string.ok, new View.OnClickListener() {
                            @Override
                            @TargetApi(Build.VERSION_CODES.M)
                            public void onClick(View v) {
                                requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_READ_EXTERNAL_STORAGE);
                            }
                        });
            } else {
                requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_READ_EXTERNAL_STORAGE);
            }
        }
    }

    void showFileChooser() {
        Log.w(TAG, "showFileChooser");
        Intent intent = new Intent();
        //sets the select file to all types of files
        intent.setType("*/*");
        //allows to select data and return it
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        //starts new activity to select file and return data
        startActivityForResult(Intent.createChooser(intent, "Choose File to Upload.."), PICK_FILE_REQUEST);
    }

    // A place has been received; use requestCode to track the request.
    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                tilCity.setText(place.getName().toString());
                LatLng coordinates = place.getLatLng(); // Get the coordinates from your place
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());

                List<Address> addresses = null; // Only retrieve 1 address
                try {
                    addresses = geocoder.getFromLocation(
                            coordinates.latitude,
                            coordinates.longitude,
                            1);
                    if (!addresses.isEmpty()) {
                        Address address = addresses.get(0);
                        String countryCode = address.getCountryCode();
                        String countryName = address.getCountryName();
                        user.getLocation().setCountry(countryCode);
                        user.getLocation().setCountryLongName(countryName);
                        // Sauvegarder l'identifiant du lieu
                        tokenManager.savePlaceId(place.getId());
                    }


                } catch (IOException e) {
                    Log.e(TAG, "onActivityResult: " + e.getMessage());
                }

                user.getLocation().setCity(place.getName().toString());
                tokenManager.saveUser(user);
                Log.i(TAG, "Place: " + place.getName());

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Toast.makeText(getApplicationContext(), status.getStatusMessage(), Toast.LENGTH_SHORT).show();

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        } else if (requestCode == PICK_FILE_REQUEST) {
            if (resultCode == RESULT_OK) {
                if (data == null) {
                    return;
                }
                Uri selectedFileUri = data.getData();
                selectedFilePath = FilePath.getPath(this, selectedFileUri);
                Log.w(TAG, "Selected file path: " + selectedFilePath);

                if (selectedFilePath != null && !selectedFilePath.isEmpty()) {
                    File file = new File(selectedFilePath);

                    GlideApp.with(this)
                            .load(file)
                            .apply(new RequestOptions().format(DecodeFormat.PREFER_RGB_565))
                            .transition(withCrossFade())
                            .transform(new CircleTransform(this))
                            .into(profilePictureView);
                    if(!accept(file)) {
                        Toast.makeText(this, "You must provide an image", Toast.LENGTH_LONG).show();
                        return;
                    }
                    RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                    MultipartBody.Part body = MultipartBody.Part.createFormData("upload", file.getName(), reqFile);
                    RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "upload_test");

                    retrofit2.Call<User> req = service.postImage(user.getId(), body, name);
                    req.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {
                            Log.w(TAG, "File upload: " + response.body());
                            if (response.isSuccessful()) {
                                Log.w(TAG, "file upload success");
                                user.setPicture(response.body().getPicture());
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Glide.get(getApplicationContext()).clearDiskCache();
                                    }
                                }).start();

                                GlideApp.with(EditProfileActivity.this)
                                        .load(selectedFilePath)
                                        //.signature(new ObjectKey(tokenManager.getUUID()))
                                        .apply(new RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                                        .transition(withCrossFade())
                                        .transform(new CircleTransform(EditProfileActivity.this))
                                        .into(profilePictureView);

                            } else {
                                ApiError apiError = Utils.convertErrors(response.errorBody());
                                Log.w(TAG, "onResponse: no success " + apiError.getMessage());
                                Toast.makeText(EditProfileActivity.this, "Image upload error", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            Log.e(TAG, "onFailure file upload: " + t.getMessage());
                            Toast.makeText(EditProfileActivity.this, "Image upload error", Toast.LENGTH_LONG).show();
                        }
                    });

                    Glide.get(getApplicationContext()).clearMemory();
                } else {
                    Toast.makeText(this, "Cannot upload file to server", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public LatLngBounds toBounds(LatLng center, double radiusInMeters) {
        double distanceFromCenterToCorner = radiusInMeters * Math.sqrt(2.0);
        LatLng southwestCorner =
                SphericalUtil.computeOffset(center, distanceFromCenterToCorner, 225.0);
        LatLng northeastCorner =
                SphericalUtil.computeOffset(center, distanceFromCenterToCorner, 45.0);
        return new LatLngBounds(southwestCorner, northeastCorner);
    }

    private boolean mayRequestPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(ACCESS_COARSE_LOCATION) || shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
            Snackbar.make(editProfilell, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION}, REQUEST_ACCESS_LOCATION);
                        }
                    });
        } else {
            requestPermissions(new String[]{ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION}, REQUEST_ACCESS_LOCATION);
        }
        return false;
    }

    private void initLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            mayRequestPermission();
        } else {
            mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {
                        BOUNDS = toBounds(new LatLng(location.getLatitude(), location.getLongitude()), RADIUS);
                        updateLocationData(location);
                        Log.w(TAG, "initlocation onSuccess: " + location.getLatitude() + ", " + location.getLongitude() + ", " + COUNTRY);
                    }
                    // canUpdate = true;

                }
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_ACCESS_LOCATION) {
            if (grantResults.length > 0) {
                boolean finePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                boolean coarsePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (finePermission && coarsePermission) {
                    initLocation();
                }
            }
        } else if (requestCode == REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showFileChooser();
                }
            }
        }

    }

    protected void createLocationRequest() {
       // mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (canUpdate)
            startLocationUpdate();
    }

    private void startLocationUpdate() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            mayRequestPermission();
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
    }

    private void updateLocationData(Location location) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String errorMessage = "";
        List<Address> addresses = null;
        // Log.w(TAG, "onSuccess: " + location);

        try {
            if (location != null)
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
        } catch (IOException e) {
            errorMessage = "service not available";
            Log.e(TAG, errorMessage, e);

        } catch (IllegalArgumentException e) {
            errorMessage = "invalid long, lat used";
            Log.e(TAG, errorMessage + ". " +
                    "Latitude = " + location.getLatitude() +
                    ", Longitude = " +
                    location.getLongitude(), e);
        }

        if (addresses == null || addresses.size() == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = "no address found";
            }
        } else {
            Address address = addresses.get(0);
            COUNTRY = address.getCountryCode();
            typeFilter = new AutocompleteFilter.Builder()
                    .setCountry(COUNTRY)
                    .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                    .build();
            ArrayList<String> addressFragments = new ArrayList<>();
            // Fetch the address lines using getAddressLine,
            // join them, and send them to the thread.
            for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                addressFragments.add(address.getAddressLine(i));
            }
            Log.i(TAG, "address found");

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (call != null) {
            call.cancel();
            call = null;
        }
    }


    @Override
    public void onDateSet(com.tsongkha.spinnerdatepicker.DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar newDate = Calendar.getInstance();
        newDate.set(year, monthOfYear, dayOfMonth);
        tilBirthday.setText(dateFormat.format(newDate.getTime()));


        String d = new SimpleDateFormat("yyyy-MM-dd").format(newDate.getTime());
        Log.w(TAG, "onDateSet: " + d);
        user.setDateOfBirth(d);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_profile_options, menu);
        return true;
    }

    public boolean accept(File file)
    {
        for (String extension : okFileExtensions)
        {
            if (file.getName().toLowerCase().endsWith(extension))
            {
                return true;
            }
        }
        return false;
    }

}
