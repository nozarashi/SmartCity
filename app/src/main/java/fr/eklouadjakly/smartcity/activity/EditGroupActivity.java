package fr.eklouadjakly.smartcity.activity;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.eklouadjakly.smartcity.GlideApp;
import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.model.Group;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.ApiError;
import fr.eklouadjakly.smartcity.network.ApiService;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.CircleTransform;
import fr.eklouadjakly.smartcity.utils.FilePath;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import fr.eklouadjakly.smartcity.utils.Utils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class EditGroupActivity extends AppCompatActivity {

    private static final String TAG = "EditGroupActivity";
    private static final int PICK_FILE_REQUEST = 1;
    private static final int REQUEST_READ_EXTERNAL_STORAGE = 2;
    private String selectedFilePath;
    private final String[] okFileExtensions = new String[]{"jpg", "png", "gif", "jpeg"};

    @BindView(R.id.edit_group_ll)
    LinearLayout editGroupll;
    @BindView(R.id.groupPicture)
    ImageView groupPictureView;
    @BindView(R.id.group_name)
    TextInputLayout tilName;
    @BindView(R.id.group_description)
    TextInputLayout tilDescription;
    @BindView(R.id.is_public)
    CheckBox checkIsPublic;

    Toolbar toolbar;
    TokenManager tokenManager;
    User user;
    ApiService service;
    Call<Group> call;
    Group group;
    AwesomeValidation validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_group);

        group = new Gson().fromJson(getIntent().getStringExtra(GroupActivity.GROUP_EDIT), Group.class);

        ButterKnife.bind(this);

        //Setup toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(group.getName());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        String profileUrl = RetrofitBuilder.getRemoteUrl() + "img/" + group.getImage();

        //Initialisation
        GlideApp.with(this)
                .load(profileUrl)
                .apply(new RequestOptions().format(DecodeFormat.PREFER_RGB_565))
                .transition(withCrossFade())
                .transform(new CircleTransform(this))
                .into(groupPictureView);

        tilName.getEditText().setText(group.getName());
        tilDescription.getEditText().setText(group.getDescription());

        tokenManager = TokenManager.getInstance(getSharedPreferences("prefs", MODE_PRIVATE));
        user = tokenManager.getUser();
        service = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);
        validator = new AwesomeValidation(ValidationStyle.TEXT_INPUT_LAYOUT);
        setupRules();
    }

    public void setupRules() {
        validator.addValidation(this, R.id.group_name, RegexTemplate.NOT_EMPTY, R.string.error_field_required);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_profile_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.save_profile:
                saveProfile();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void saveProfile() {
        tilName.setError(null);
        validator.clear();
        if (validator.validate()) {
            group.setName(tilName.getEditText().getText().toString());
            group.setDescription(tilDescription.getEditText().getText().toString());
            group.setPublic(checkIsPublic.isChecked());
            call = service.patchGroup(group.getId(), group.getName(), group.getDescription(), group.getPublic());
            call.enqueue(new Callback<Group>() {
                @Override
                public void onResponse(Call<Group> call, Response<Group> response) {
                    if (response.isSuccessful()) {
                        Log.w(TAG, "onResponse success: " + response.body());


                        if (selectedFilePath != null && !selectedFilePath.isEmpty()) {
                            long id = response.body().getId();
                            File file = new File(selectedFilePath);
                            if (!accept(file)) {
                                Toast.makeText(EditGroupActivity.this, "You must provide an image", Toast.LENGTH_LONG).show();
                                return;
                            }
                            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                            MultipartBody.Part body = MultipartBody.Part.createFormData("upload", file.getName(), reqFile);
                            RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "upload_test");
                            retrofit2.Call<Group> req = service.postGroupImage(user.getId(), id, body, name);
                            req.enqueue(new Callback<Group>() {
                                @Override
                                public void onResponse(Call<Group> call, Response<Group> response) {
                                    if (response.isSuccessful()) {
                                        Log.w(TAG, "file upload: success");
                                        Intent intent = new Intent();
                                        intent.putExtra(GroupActivity.GROUP_EDIT, new Gson().toJson(response.body()));
                                      /*  new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Glide.get(EditGroupActivity.this).clearDiskCache();
                                            }
                                        }).start();
                                        Glide.get(getApplicationContext()).clearMemory();*/

                                        setResult(RESULT_OK, intent);
                                        Toast.makeText(EditGroupActivity.this, "Modifications sauvegardées", Toast.LENGTH_LONG).show();
                                        finish();
                                    } else {
                                        ApiError apiError_ = Utils.convertErrors(response.errorBody());
                                        Log.w(TAG, "file upload: no success " + apiError_.getMessage());
                                        Toast.makeText(EditGroupActivity.this, apiError_.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<Group> call, Throwable t) {
                                    Log.w(TAG, "file upload: failed");
                                    Toast.makeText(EditGroupActivity.this, "An error occured", Toast.LENGTH_LONG).show();
                                }
                            });

                        } else {
                            Toast.makeText(EditGroupActivity.this, "Modifications sauvegardées", Toast.LENGTH_LONG).show();
                            finish();
                        }

                    } else {
                        ApiError apiError = Utils.convertErrors(response.errorBody());
                        Log.w(TAG, "onResponse failure: " + apiError.getMessage() + " " + response.code());
                        Toast.makeText(EditGroupActivity.this, apiError.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Group> call, Throwable t) {
                    Log.w(TAG, "onFailure: " + t.getMessage());
                    Toast.makeText(EditGroupActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @OnClick(R.id.attachment)
    void upload() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            showFileChooser();
        } else if (checkSelfPermission(READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            showFileChooser();
        } else {
            if (shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE)) {
                Snackbar.make(editGroupll, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                        .setAction(android.R.string.ok, new View.OnClickListener() {
                            @Override
                            @TargetApi(Build.VERSION_CODES.M)
                            public void onClick(View v) {
                                requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_READ_EXTERNAL_STORAGE);
                            }
                        });
            } else {
                requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_READ_EXTERNAL_STORAGE);
            }
        }
    }

    void showFileChooser() {
        Log.w(TAG, "showFileChooser");
        Intent intent = new Intent();
        //sets the select file to all types of files
        intent.setType("*/*");
        //allows to select data and return it
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        //starts new activity to select file and return data
        startActivityForResult(Intent.createChooser(intent, "Choose File to Upload.."), PICK_FILE_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showFileChooser();
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_FILE_REQUEST) {
            if (resultCode == RESULT_OK) {
                if (data == null) {
                    return;
                }
                Uri selectedFileUri = data.getData();
                selectedFilePath = FilePath.getPath(this, selectedFileUri);
                Log.w(TAG, "Selected file path: " + selectedFilePath);

                File file = new File(selectedFilePath);

                GlideApp.with(this)
                        .load(file)
                        .apply(new RequestOptions().format(DecodeFormat.PREFER_RGB_565))
                        .transition(withCrossFade())
                        .transform(new CircleTransform(this))
                        .into(groupPictureView);

            }
        }
    }

    public boolean accept(File file) {
        for (String extension : okFileExtensions) {
            if (file.getName().toLowerCase().endsWith(extension)) {
                return true;
            }
        }
        return false;
    }
}
