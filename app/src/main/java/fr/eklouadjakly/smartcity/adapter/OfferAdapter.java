package fr.eklouadjakly.smartcity.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;

import java.util.List;

import fr.eklouadjakly.smartcity.GlideApp;
import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.model.Interest;
import fr.eklouadjakly.smartcity.model.Offer;
import fr.eklouadjakly.smartcity.model.Trading;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.ApiService;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.CircleTransform;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import retrofit2.Call;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by Faniry on 01/05/2018.
 */

public class OfferAdapter extends RecyclerView.Adapter<OfferAdapter.MyViewHolder> {

    private static final String TAG = "OfferAdapter";
    private Context context;
    private List<Offer> data;
    private List<Offer> offers = null;
    String profileUrl;
    protected TokenManager tokenManager;
    private User user;
    ApiService service;
    Call<Offer> call;

    public OfferAdapter(Context context, List<Offer> data) {
        this.context = context;
        this.data = data;

        tokenManager = TokenManager.getInstance(context.getSharedPreferences("prefs", Context.MODE_PRIVATE));
        user = tokenManager.getUser();
        service = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);
    }

    @NonNull
    public OfferAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.activity_shop_offer_item, parent, false);
        final OfferAdapter.MyViewHolder viewHolder = new OfferAdapter.MyViewHolder(view);

        return viewHolder;
    }

    public void onBindViewHolder(@NonNull final OfferAdapter.MyViewHolder holder, final int position) {
        profileUrl = RetrofitBuilder.getRemoteUrl() + "img/" + data.get(position).getImage();
        String buffer;
        Interest interet;
        Trading tradingvar;

        //on rempli MyViewHolder
        buffer = data.get(position).getProduct();
        holder.product.setText(buffer);
        Log.w(TAG, "onBindViewHolder OfferAdapter Offer:  " + buffer);

        buffer = data.get(position).getBrand();
        holder.brand.setText(buffer);
        Log.w(TAG, "onBindViewHolder OfferAdapter Offer:  " + buffer);

        String prix = String.valueOf(data.get(position).getPrice()) + "€";
        holder.price.setText(prix);
        Log.w(TAG, "onBindViewHolder OfferAdapter Offer:  " + prix);

        tradingvar = data.get(position).getTrading();
        if(tradingvar != null) {
            buffer = tradingvar.getName();
            holder.trading.setText(buffer);
            Log.w(TAG, "onBindViewHolder OfferAdapter Offer:  " + buffer);

            buffer = tradingvar.getCompleteAddress();
            holder.location.setText(buffer);
            Log.w(TAG, "onBindViewHolder OfferAdapter AdressTrading:  " + buffer);
        } else {
            holder.trading.setVisibility(View.GONE);
            holder.location.setVisibility(View.GONE);
        }
        interet = data.get(position).getCategory();
        holder.interest.setText(interet.getCategory());
        Log.w(TAG, "onBindViewHolder OfferAdapter Offer:  " + buffer);

        buffer = data.get(position).getImage();
        Log.w(TAG, "onBindViewHolder OfferAdapter Offer:  " + buffer);

        if (buffer == null) {
            holder.img.setImageResource(R.drawable.logo_offer);
        }
        else {
            GlideApp.with(context)
                    .load(profileUrl)
                    .apply(new RequestOptions().format(DecodeFormat.PREFER_RGB_565))
                    .transition(withCrossFade())
                    .into(holder.img);
        }
    }

    public int getItemCount() {
        return data.size();
    }

    public List<Offer> getData() {
        return data;
    }

    public void setData(List<Offer> data) {
        this.data = data;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView img;
        TextView product;
        TextView brand;
        TextView price;
        TextView trading;
        TextView location;
        TextView interest;
        RelativeLayout viewContent;

        public MyViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.offerimg);
            product = itemView.findViewById(R.id.offer_product);
            brand = itemView.findViewById(R.id.offer_brand);
            price = itemView.findViewById(R.id.offer_price);
            trading = itemView.findViewById(R.id.offer_trading);
            location = itemView.findViewById(R.id.offer_trading_location);
            interest = itemView.findViewById(R.id.offer_trading_interest);
            viewContent = itemView.findViewById(R.id.offercontent);
        }
    }
}
