package fr.eklouadjakly.smartcity.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InterestRequest {

    public InterestRequest(List<String> interests) {
        this.interests = interests;
    }

    @SerializedName("interests")
    @Expose
    private List<String> interests = null;

    public List<String> getInterests() {
        return interests;
    }

    public void setInterests(List<String> interests) {
        this.interests = interests;
    }

}
