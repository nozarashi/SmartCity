package fr.eklouadjakly.smartcity.network;

import java.util.List;
import java.util.Map;

public class ApiError {
    private String message;
    private Map<String, List<String>> errors;

    public String getMessage() {
        return message;
    }

    public Map<String, List<String>> getErrors() {
        return errors;
    }
}
